# QuickDocs metadata mass update

What I want to do is mass update of the Fedora Docs metadata by appending the files with defined category and tags for each article.

:category: CATEGORY
:tags: TAG_01 TAG_02 TAG_03 ... TAG_n

Docs repo URL: https://pagure.io/fedora-docs/quick-docs.git

## Rough process steps

- List the files and metadata and save them onto a CSV file: script 'learnToScript.sh'
- Update metadata on a CSV file: update manually
- List the files and metadata, loop over them, append the text with echo "text" >> $file

## Authors and acknowledgment
Credit to A.Sinha

## License
MIT for software or CC BY-SA for content

https://docs.fedoraproject.org/en-US/legal/fpca/
